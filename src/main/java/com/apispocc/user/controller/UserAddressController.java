package com.apispocc.user.controller;

import com.apispocc.user.constant.ResponseType;
import com.apispocc.user.dto.UserDto;
import com.apispocc.user.entity.User;
import com.apispocc.user.dto.UserAddressDto;
import com.apispocc.user.entity.UserAddress;
import com.apispocc.user.response.Response;
import com.apispocc.user.service.UserAddressService;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
//@RequestMapping("/api/v1")
public class UserAddressController {
    @Autowired
    private UserAddressService service;
    @Autowired
    private ModelMapper modelMapper;

    @PutMapping("/users/{userId}/addresses/{userAddId}")
    public ResponseEntity<Response> updateAddress(@PathVariable(value = "userId") Long userId,
                                     @PathVariable (value = "userAddId") Long userAddId,
                                     @Valid @RequestBody UserAddressDto userAddress) throws Exception {
        try {
            UserAddress addressRequest=modelMapper.map(userAddress,UserAddress.class);
            UserAddress addressResponse=service.updateAddress(userId,userAddId,addressRequest);
            UserAddressDto result=modelMapper.map(addressResponse,UserAddressDto.class);
            Response response=Response.builder()
                    .type(ResponseType.SUCCESS)
                    .data(result)
                    .message("Updated")
                    .statusCode(HttpStatus.OK.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.OK);
        } catch (Exception e) {
            Response response=Response.builder()
                    .type(ResponseType.ERROR)
                    .data(null)
                    .message(e.getMessage())
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
    }
    }


    @PostMapping("/users/{userId}/addresses")
    public ResponseEntity<Response> createAddress(@PathVariable (value = "userId") Long userId,
                                                       @Valid @RequestBody UserAddressDto userAddress) throws Exception {
        try {
            //modelMapper.getConfiguration().setAmbiguityIgnored(true);
            // convert DTO to entity
            UserAddress addressRequest=modelMapper.map(userAddress,UserAddress.class);
            User user = service.createAddress(userId,addressRequest);
            // convert entity to DTO
            UserDto userResponse = modelMapper.map(user, UserDto.class);
            Response response=Response.builder()
                    .type(ResponseType.SUCCESS)
                    .data(userResponse)
                    .message("Successfully added data!")
                    .statusCode(HttpStatus.CREATED.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.CREATED);
        } catch (Exception e) {
            Response response=Response.builder()
                    .type(ResponseType.ERROR)
                    .data(null)
                    .message(e.getMessage())
                    .statusCode(HttpStatus.BAD_REQUEST.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
        }
    }
}
