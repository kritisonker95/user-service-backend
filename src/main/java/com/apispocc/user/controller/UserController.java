package com.apispocc.user.controller;

import com.apispocc.user.constant.ResponseType;
import com.apispocc.user.dto.UserDetailUpdateDto;
import com.apispocc.user.dto.UserDto;
import com.apispocc.user.entity.User;
import com.apispocc.user.response.Response;
import com.apispocc.user.service.UserService;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
//@RequestMapping("/api/v1")
public class UserController {
    @Autowired
    private UserService service;

    @Autowired
    private ModelMapper modelMapper;

    /**
     * Post request to register a new user or create a new user in the database
     * @param userDto
     * @return
     * @throws Exception
     */
    @PostMapping("/register")
    public ResponseEntity<Response> addUser(@Valid @RequestBody UserDto userDto) throws  Exception{
        try {
            //modelMapper.getConfiguration().setAmbiguityIgnored(true);
            // convert DTO to entity
            UserDto savedUserDto = service.saveUser(userDto);
            Response response=Response.builder()
                    .type(ResponseType.SUCCESS)
                    .data(savedUserDto)
                    .message("Successfully added data!")
                    .statusCode(HttpStatus.CREATED.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.CREATED);
        } catch (Exception e) {
            Response response=Response.builder()
                    .type(ResponseType.ERROR)
                    .data(null)
                    .message(e.getMessage())
                    .statusCode(HttpStatus.BAD_REQUEST.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
        }
    }

    //Get
    @GetMapping("/users")
    public ResponseEntity<Response> getAllUsers(){
        try {
            //Set<UserDto> result = service.getUsers().stream().map(user -> modelMapper.map(user,UserDto.class)).collect(Collectors.toSet());

            Set<UserDto> result = service.getUsers().stream().map(user -> modelMapper.map(user,UserDto.class)).collect(Collectors.toSet());
            Response response=Response.builder()
                    .type(ResponseType.SUCCESS)
                    .data(result)
                    .message("Successfully retrieved data!")
                    .statusCode(HttpStatus.OK.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.OK);
        } catch (Exception e) {
            Response response=Response.builder()
                    .type(ResponseType.ERROR)
                    .data(null)
                    .message(e.getMessage())
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
        }
    }

    //Get By Id
    @RequestMapping(value = "/users/{userId}", method = RequestMethod.GET)
    public ResponseEntity<Response> getUserById(@PathVariable Long userId){
        try {
            User userResponse=service.getUserById(userId);
            UserDto result = modelMapper.map(userResponse,UserDto.class);
            Response response=Response.builder()
                    .type(ResponseType.SUCCESS)
                    .data(result)
                    .message("Successfully retrieved data!")
                    .statusCode(HttpStatus.OK.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.OK);
        } catch (Exception e) {
            Response response=Response.builder()
                    .type(ResponseType.ERROR)
                    .data(null)
                    .message("No user found for user id ="+userId)
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/users/creds/{userCredId}")
    public ResponseEntity<Response> getUserByCredentialId(@PathVariable long userCredId){
        try {
            UserDto userDto=service.getUserByCredentialId(userCredId);
            Response response=Response.builder()
                    .type(ResponseType.SUCCESS)
                    .data(userDto)
                    .message("Successfully retrieved data!")
                    .statusCode(HttpStatus.OK.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.OK);
        } catch (Exception e) {
            Response response=Response.builder()
                    .type(ResponseType.ERROR)
                    .data(null)
                    .message("No user found for user cred id ="+userCredId)
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
        }
    }

    // Update user details
    @PutMapping(value = "/users/{userId}/detail")
    public ResponseEntity<Response> UpdateDetail(@PathVariable Long userId, @Valid @RequestBody UserDetailUpdateDto userDto) {
        try {
            UserDetailUpdateDto result = service.updateUserDetail(userId,userDto);
            Response response=Response.builder()
                    .type(ResponseType.SUCCESS)
                    .data(result)
                    .message("Updated")
                    .statusCode(HttpStatus.OK.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.OK);
        } catch (Exception e) {
            Response response=Response.builder()
                    .type(ResponseType.ERROR)
                    .data(null)
                    .message(e.getMessage())
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
        }
    }

    //Delete
    @DeleteMapping("/users/{userId}")
    public ResponseEntity<Response> deleteUserById(@PathVariable Long userId){
        try {
            String result = service.deleteUser(userId);
            Response response=Response.builder()
                    .type(ResponseType.SUCCESS)
                    .data(result)
                    .message("Deleted")
                    .statusCode(HttpStatus.OK.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.OK);
        } catch (Exception e) {
            Response response=Response.builder()
                    .type(ResponseType.ERROR)
                    .data(null)
                    .message(e.getMessage())
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .timeStamp(System.currentTimeMillis())
                    .build();
            return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
        }
    }

}

