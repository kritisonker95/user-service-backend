package com.apispocc.user.service;

import com.apispocc.user.dto.UserCredentialDto;
import com.apispocc.user.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class UserCredentialServiceImpl implements  UserCredentialService{

    @Autowired
    private WebClient.Builder webBuilder;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public UserCredentialDto saveUserCredential(UserCredentialDto ucDto) {
        log.info("saveUserCredential: User cred dto : {}",ucDto);
        // saving credential to userDto credential service
        Response response = webBuilder.build()
                .post()
                .uri("http://AUTHENTICATION-SERVICE/api/v1/auth/creds")
                .body(Mono.just(ucDto), UserCredentialDto.class)
                .retrieve()
                .bodyToMono(Response.class)
                .block();
        System.out.println("Response is ------ "+response);
        assert response != null;
        return modelMapper.map(response.getData(), UserCredentialDto.class);
    }
}
