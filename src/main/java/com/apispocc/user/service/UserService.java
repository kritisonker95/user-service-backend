package com.apispocc.user.service;

import com.apispocc.user.dto.*;
import com.apispocc.user.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

public interface UserService {
    // Api-02	User Management	/users/register	POST	register a new user
    UserDto saveUser(UserDto userDto);
    //Api-06	User Management	/users	GET	get all users
    List<User> getUsers();
    //Api-07	User Management	/users/{userId}	GET	get a unique user

    /**
     * get user by userId
     * @param userId
     * @return
     */
    User getUserById(Long userId);
    //Api-09	User Management	/users/{userId}/detail	PUT	update a user's detail
    UserDetailUpdateDto updateUserDetail(Long userId, UserDetailUpdateDto userDto);

   //Api-10	User Management	/users/{userId}	DELETE	delete a  user
    String deleteUser(Long userId);

    UserDto getUserByCredentialId(long userCredId);
}
