package com.apispocc.user.service;

import com.apispocc.user.dto.UserAddressDto;
import com.apispocc.user.entity.User;
import com.apispocc.user.entity.UserAddress;
import jakarta.validation.Valid;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

public interface UserAddressService {
    //Api-08	User Management	/users/{userId}/addresses/{userAddId}	PUT	-update an existing user's address
    UserAddress updateAddress(Long userId,Long userAddId,UserAddress addressRequest) throws Exception;
    //Api-32	User Management /users/{userId}/addresses POST - add a new address for an existing user
    User createAddress(Long userId,UserAddress address) throws  Exception;
}
