package com.apispocc.user.service;

import com.apispocc.user.dto.UserCredentialDto;
import com.apispocc.user.dto.UserDetailUpdateDto;
import com.apispocc.user.dto.UserDto;
import com.apispocc.user.entity.User;
import com.apispocc.user.repository.UserRepository;
import com.apispocc.user.response.Response;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserCredentialService userCredentialService;

    @Override
    public UserDto saveUser(UserDto userDto) {
        log.info("saveUser: Saving user : {}",userDto);
        UserCredentialDto ucDto = userDto.getUserCredential();
        log.info("saveUser: credentials : {}",ucDto);
        UserCredentialDto userCredentialDto = userCredentialService.saveUserCredential(ucDto);
        User user = modelMapper.map(userDto, User.class);
        user.setUserCredentialId(userCredentialDto.getUserCredId());
        User savedUser = userRepository.save(user);
        log.info("before error model mapper");
        UserDto savedUserDto = modelMapper.map(savedUser, UserDto.class);
        log.info("after error model mapper");
        savedUserDto.setUserCredential(userCredentialDto);
        return savedUserDto;
    }

    @Override
    public List<User> getUsers() {
        List<User> users=userRepository.findAll();
        return users;
    }

    @Override
    public User getUserById(Long userId) {
        User user=userRepository.findById(userId).orElse(null);
        return  user;
    }

    @Override
    public UserDetailUpdateDto updateUserDetail(Long userId, UserDetailUpdateDto userDto) {
        User existingUser =userRepository.findById(userId).orElse(null);
        existingUser.setFirstName(userDto.getFirstName());
        existingUser.setLastName(userDto.getLastName());
        existingUser.setDob(userDto.getDob());
        existingUser.setGender(userDto.getGender());
        existingUser.setMobile(userDto.getMobile());
        existingUser.setTargetSteps(userDto.getTargetSteps());
        User userResult=userRepository.save(existingUser);
        UserDetailUpdateDto userDtoResult=modelMapper.map(userResult,UserDetailUpdateDto.class);
        return userDtoResult;
    }

    @Override
    public String deleteUser(Long userId) {
        userRepository.deleteById(userId);
        return "User removed with id "+userId;
    }

    @Override
    public UserDto getUserByCredentialId(long userCredId) {
        User user = userRepository.findByUserCredentialId(userCredId)
                .orElseThrow(() -> new RuntimeException("User not found for cred id: "+ userCredId));
        return modelMapper.map(user, UserDto.class);
    }

    //Api-11	User Management	/users/{userId}/change-password	PUT	update a  user's password
//    @Override
//    public User updateUserPassword(Long userId, User user) {
//        User existingUser =userRepository.findById(userId).orElse(null);
//        existingUser.setUserCredential(user.getUserCredential());
//        return userRepository.save(existingUser);
//    }
}
