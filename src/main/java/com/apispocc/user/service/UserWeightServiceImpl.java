package com.apispocc.user.service;

import com.apispocc.user.entity.UserWeight;
import com.apispocc.user.repository.UserWeightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserWeightServiceImpl implements UserWeightService{
    @Autowired
    UserWeightRepository userWeightRepository;
    @Override
    public UserWeight saveUserWeight(UserWeight userWeight) {
        return userWeightRepository.save(userWeight);
    }
}
