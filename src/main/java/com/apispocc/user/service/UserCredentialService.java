package com.apispocc.user.service;

import com.apispocc.user.dto.UserCredentialDto;

public interface UserCredentialService {
    UserCredentialDto saveUserCredential(UserCredentialDto userCredential);

}
