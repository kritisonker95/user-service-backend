package com.apispocc.user.service;

import com.apispocc.user.dto.UserAddressDto;
import com.apispocc.user.entity.User;
import com.apispocc.user.entity.UserAddress;
import com.apispocc.user.repository.UserAddressRepository;
import com.apispocc.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserAddressServiceImpl implements UserAddressService{
    @Autowired
    private UserAddressRepository userAddressRepo;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private ModelMapper modelMapper;
    @Override
    public UserAddress updateAddress(Long userId, Long userAddId, UserAddress addressRequest) throws Exception{
        if(!userRepo.existsById(userId)) {
            throw new Exception("userId "+userId+" not found");
        }

        return userAddressRepo.findById(userAddId).map(address -> {
            address.setCountry(addressRequest.getCountry());
            address.setLandmark(addressRequest.getLandmark());
            address.setPincode(addressRequest.getPincode());
            address.setStreet(addressRequest.getStreet());
            address.setCity(addressRequest.getCity());
            return userAddressRepo.save(address);
        }).orElseThrow(() -> new Exception("UserAddressId " + userAddId + "not found"));
    }

    @Override
    public User createAddress(Long userId, UserAddress address) throws Exception{
        UserAddress addressRequest=modelMapper.map(address,UserAddress.class);
        User userTemp= userRepo.findById(userId).map(user -> {
            user.getAddresses().add(addressRequest);
            return userRepo.save(user);
        }).orElseThrow(() -> new Exception("UserId " + userId + " not found"));
        return  userTemp;
    }
}
