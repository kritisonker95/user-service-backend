package com.apispocc.user.service;

import com.apispocc.user.entity.UserWeight;

public interface UserWeightService {
    UserWeight saveUserWeight(UserWeight userWeight);
}
