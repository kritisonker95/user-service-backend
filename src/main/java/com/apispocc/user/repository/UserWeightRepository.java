package com.apispocc.user.repository;

import com.apispocc.user.entity.UserWeight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserWeightRepository extends JpaRepository<UserWeight,Long> {
}
