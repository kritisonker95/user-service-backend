/**
 * Project: fitbuddy-user-service
 * Package: package com.apispocc.user.response
 * <p>
 * @author : Kavita Rana
 * Date: 04/27/2023
 * <p>
 */

package com.apispocc.user.response;

import com.apispocc.user.constant.ResponseType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 */


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Response {
    private ResponseType type;
    private String message;
    private int statusCode;
    private Object data;
    private Long timeStamp;
}
