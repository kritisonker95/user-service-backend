package com.apispocc.user.dto;

import com.apispocc.user.constant.City;
import com.apispocc.user.constant.Country;
import com.apispocc.user.constant.State;
import com.apispocc.user.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserAddressDto {
    private Long userAddressId;
    @NotNull
    private String street;
    @NotNull
    private City city;
    @NotNull
    private State state;
    @NotNull
    private int pincode;
    private String landmark;
    @NotNull
    private Country country;
}
