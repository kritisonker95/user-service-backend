package com.apispocc.user.dto;

import com.apispocc.user.constant.City;
import com.apispocc.user.constant.Country;
import com.apispocc.user.constant.State;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserAddressUpdateDto {
    private String street;
    private City city;
    private State state;
    private int pincode;
    private String landmark;
    private Country country;
}
