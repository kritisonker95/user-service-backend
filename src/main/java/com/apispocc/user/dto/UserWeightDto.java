package com.apispocc.user.dto;

import com.apispocc.user.entity.User;
import jakarta.persistence.Column;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserWeightDto {
    private Long userWtId;
    @NotNull
    private Long wtCurrent;
    @NotNull
    private Long wtGoal;
    protected Date createdOn;
    protected Date updatedOn;
}

