package com.apispocc.user.dto;

import com.apispocc.user.constant.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private Long id;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    private LocalDate dob;
    @NotNull
    private Gender gender;
    private String mobile;
    @NotNull
    private int targetSteps;
    private Set<UserAddressDto> addresses = new HashSet<>();
    private UserWeightDto userWeight ;
    //Set<UserStepsDto> steps = new HashSet<>();
    private UserCredentialDto userCredential;
    protected Date createdOn;
    protected Date updatedOn;
}

