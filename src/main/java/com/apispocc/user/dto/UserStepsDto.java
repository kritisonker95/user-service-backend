package com.apispocc.user.dto;

import jakarta.persistence.Column;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserStepsDto {
    private Long userStepsId;
    @NotNull
    private Long stepCount;
    protected Date createdOn;
    protected Date updatedOn;
}
