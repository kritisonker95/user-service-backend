package com.apispocc.user.dto;

import com.apispocc.user.constant.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailUpdateDto {
    private String firstName;
    private String lastName;
    private LocalDate dob;
    private Gender gender;
    private String mobile;
    private int targetSteps;
}
