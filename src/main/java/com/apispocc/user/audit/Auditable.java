package com.apispocc.user.audit;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import jakarta.persistence.*;
import java.util.Date;

/**
 *  Project: fitbuddy-user-service
 *  Package: com.apispocc.user.audit;
 *  @author KavitaJRana
 *  Usage: to audit the entity (whichever inherits this class) as it changes on db change
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Auditable{
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_on",columnDefinition = "timestamp default '2023-03-31 20:06:05.967394'", nullable = false,updatable = false)
    protected Date createdOn;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name ="updated_on",columnDefinition = "timestamp default '2023-03-31 20:06:05.967394'",nullable = false)
    protected Date updatedOn;

}

