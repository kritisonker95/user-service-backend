package com.apispocc.user.entity;

import com.apispocc.user.audit.Auditable;
import com.apispocc.user.constant.Gender;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import jakarta.persistence.*;
import org.springframework.data.domain.Persistable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * Project: fitbuddy-user-service
 * Package: com.apispocc.user.entity
 * @author KavitaJRana
 * Usage: User Entity is used to store details of a User present in the application
 */

@NoArgsConstructor
@AllArgsConstructor
@Entity
@DynamicUpdate
@Getter
@Setter
@Table(name = "user",schema = "UserManagement")
public class User extends Auditable{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "user_id")
    private Long id;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    Set<UserAddress> addresses = new HashSet<>();


    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_wt_id",referencedColumnName = "user_wt_id")
    private UserWeight userWeight;

//    @OneToMany(cascade = CascadeType.ALL)
//    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
//    Set<UserSteps> steps = new HashSet<>();

    private long userCredentialId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "dob")
    private LocalDate dob;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "mobile")
    private String mobile;

    @Column(name="target_steps",columnDefinition = "int default 10000")
    private int targetSteps;

    @Column(name = "last_logged_in")
    private LocalDateTime lastLoggedIn=LocalDateTime.now();

    @Column(name = "is_active")
    private boolean isActive=true;

    @Override
    public String toString() {
        return "User{" +
                "userId=" + id +
                ", addresses=" + addresses +
                ", userWeight=" + userWeight +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dob=" + dob +
                ", gender=" + gender +
                ", mobile='" + mobile + '\'' +
                ", targetSteps=" + targetSteps +
                ", lastLoggedIn=" + lastLoggedIn +
                ", isActive=" + isActive +
                '}';
    }

}


