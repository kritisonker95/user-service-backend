package com.apispocc.user.entity;

import com.apispocc.user.audit.Auditable;
import jakarta.persistence.*;
import lombok.*;

/**
 * Project: fitbuddy-be
 * Package: com.apispocc.user.entity
 * @author KavitaJRana
 * Usage: UserSteps Entity is used to store steps details of a User present in the application
 */

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user_steps",schema = "UserManagement")
public class UserSteps extends Auditable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "user_steps_id")
    private Long userStepsId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "step_count")
    private Long stepCount;

    @Override
    public String toString() {
        return "UserSteps{" +
                "userStepsId=" + userStepsId +
                ", userId=" + userId +
                ", stepCount=" + stepCount +
                '}';
    }
}
