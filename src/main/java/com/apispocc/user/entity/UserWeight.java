package com.apispocc.user.entity;

import com.apispocc.user.audit.Auditable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

/**
 * Project: fitbuddy-be
 * Package: com.apispocc.user.entity
 * @author KavitaJRana
 * Usage: UserWeight Entity is used to store weight details of a User present in the application
 */

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user_weight")
public class UserWeight extends Auditable{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "user_wt_id")
    private Long userWtId;

    @OneToOne(mappedBy = "userWeight")
    private User user;

    @Column(name = "wt_current")
    private Long wtCurrent;

    @Column(name = "wt_goal")
    private Long wtGoal;

    //removed user from here to break the loop that was causing stack overflow
    @Override
    public String toString() {
        return "UserWeight{" +
                "userWtId=" + userWtId +
                ", wtCurrent=" + wtCurrent +
                ", wtGoal=" + wtGoal +
                '}';
    }
}

