package com.apispocc.user.entity;


import com.apispocc.user.audit.Auditable;
import com.apispocc.user.constant.City;
import com.apispocc.user.constant.Country;
import com.apispocc.user.constant.State;
import lombok.*;
import jakarta.persistence.*;

/**
 * Project: fitbuddy-be
 * Package: com.apispocc.user.entity
 * @author KavitaJRana
 * Usage: UserAddress Entity is used to store address details of a User present in the application
 */

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "user_address",schema = "UserManagement")
public class UserAddress extends Auditable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "user_add_id")
    private Long userAddressId;

    @Column(name = "street")
    private String street;

    @Column(name = "city")
    @Enumerated(EnumType.STRING)
    private City city;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private State state;

    @Column(name = "pincode")
    private int pincode;

    @Column(name = "landmark")
    private String landmark;

    @Column(name = "country")
    @Enumerated(EnumType.STRING)
    private Country country;

    @Column(name = "is_active")
    private boolean isActive=true;


    @Override
    public String toString() {
        return "UserAddress{" +
                "userAddressId=" + userAddressId +
                ", street='" + street + '\'' +
                ", city=" + city +
                ", state=" + state +
                ", pincode=" + pincode +
                ", landmark='" + landmark + '\'' +
                ", country=" + country +
                ", isActive=" + isActive +
                '}';
    }
}

