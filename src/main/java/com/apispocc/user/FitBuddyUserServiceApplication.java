package com.apispocc.user;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.reactive.function.client.WebClient;


/**
 * Project: fitbuddy-user-service
 * Package: com.apispocc.user
 * @author KavitaJRana
 * Usage: User Service can be used as a User Management Application in order to carry out all the basic or customisable
 * operations related to a user
 */
@EnableJpaAuditing
@SpringBootApplication
@EnableDiscoveryClient
public class FitBuddyUserServiceApplication {
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Bean
	@LoadBalanced
	public WebClient.Builder builder() {
		return WebClient.builder();
	}

	public static void main(String[] args) {
		SpringApplication.run(FitBuddyUserServiceApplication.class, args);
	}
}
