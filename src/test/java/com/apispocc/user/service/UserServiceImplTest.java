package com.apispocc.user.service;

import com.apispocc.user.constant.City;
import com.apispocc.user.constant.Country;
import com.apispocc.user.constant.Gender;
import com.apispocc.user.constant.State;
import com.apispocc.user.dto.*;
import com.apispocc.user.entity.User;
import com.apispocc.user.entity.UserAddress;
import com.apispocc.user.entity.UserWeight;
import com.apispocc.user.helper.UserServiceHelper;
import com.apispocc.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@Slf4j
class UserServiceImplTest {
    @Mock
    private UserRepository userRepository;
    @Spy
    private final ModelMapper modelMapper = new ModelMapper();
    @Mock
    private UserCredentialService userCredentialService;
    @InjectMocks
    private UserServiceImpl userService;

    @BeforeEach
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    // JUnit test for saveUser method ,this method was giving errors , need to look into it
    @DisplayName("JUnit test for saveUser method")
    @Test
    public void givenUserObject_whenSaveUser_thenReturnUserObject() throws  Exception{
        //address dto
        Set<UserAddressDto> addressesDto=new HashSet<>();
        UserAddressDto addressDto=new UserAddressDto();
        addressDto.setUserAddressId(1L);
        addressDto.setStreet("Kanarichhina,Nougaon");
        addressDto.setCity(City.ALMORA);
        addressDto.setState(State.UTTARAKHAND);
        addressDto.setPincode(263624);
        addressDto.setLandmark("Near Dhaulchhina");
        addressDto.setCountry(Country.INDIA);
        addressesDto.add(addressDto);
        //weight dto
        UserWeightDto userWeightDto=new UserWeightDto();
        userWeightDto.setUserWtId(1L);
        userWeightDto.setWtCurrent(55L);
        userWeightDto.setWtGoal(53L);
        //cred dto
        UserCredentialDto userCredentialDto=new UserCredentialDto();
        userCredentialDto.setUserCredId(1L);
        userCredentialDto.setUsername("KavitaRana2406");
        userCredentialDto.setPassword("Kavita@24");
        userCredentialDto.setEmail("ranakavi2406@gmail.com");
        //user given to service
        UserDto userDto=new UserDto();
        userDto.setId(1L);
        userDto.setFirstName("Kavita");
        userDto.setLastName("Rana");
        userDto.setDob(LocalDate.of(1999, Month.JUNE, 24));
        userDto.setGender(Gender.FEMALE);
        userDto.setMobile("8433011250");
        userDto.setTargetSteps(12000);
        userDto.setAddresses(addressesDto);//put set of address dto
        userDto.setUserWeight(userWeightDto);//put weight dto
        userDto.setUserCredential(userCredentialDto);//put cred dto


        lenient().when(userCredentialService.saveUserCredential(userCredentialDto)).thenReturn(userCredentialDto);

        // address
        Set<UserAddress> addresses=new HashSet<>();
        UserAddress address=new UserAddress();
        address.setUserAddressId(1L);
        address.setStreet("Kanarichhina,Nougaon");
        address.setCity(City.ALMORA);
        address.setState(State.UTTARAKHAND);
        address.setPincode(263624);
        address.setLandmark("Near Dhaulchhina");
        address.setCountry(Country.INDIA);
        addresses.add(address);

        //weight
        UserWeight userWeight=new UserWeight();
        userWeight.setUserWtId(1L);
        userWeight.setWtCurrent(55L);
        userWeight.setWtGoal(53L);

        // user given to userRepo
        User user=new User();
        user.setId(1L);
        user.setFirstName("Kavita");
        user.setLastName("Rana");
        user.setDob(LocalDate.of(1999, Month.JUNE, 24));
        user.setGender(Gender.FEMALE);
        user.setMobile("8433011250");
        user.setTargetSteps(12000);
        user.setAddresses(addresses);//put set of address
        user.setUserWeight(userWeight);
        user.setUserCredentialId(userCredentialDto.getUserCredId());

        lenient().when(userRepository.save(user)).thenReturn(user);


//        when(userService.saveUser(any(UserDto.class))).thenReturn(userDto);

    }


    // JUnit test for getUsers method
    @DisplayName("JUnit test for getUsers method")
    @Test
    void givenUsersList_whenGetUsers_thenReturnUsersList() {
        //given - List of Users will be returned with repo call
        List<User> users= UserServiceHelper.getDummyUserList(); // dummy list to be returned
        when(userRepository.findAll()).thenReturn(users);//mocking call to repo
        // when -actual call to the service's method
        List<User> userList = userService.getUsers();
        //then
        assertThat(userList).isNotNull();
        assertThat(userList.size()).isEqualTo(2);
    }

    // JUnit test for getUsers method
    @DisplayName("JUnit test for getUsers method (negative scenario)")
    @Test
    void givenUsersList_whenGetUsers_thenReturnEmptyUsersList() {
        //given - List of Users will be returned with repo call
        List<User> users= UserServiceHelper.getDummyUserList(); // dummy list to be returned
        when(userRepository.findAll()).thenReturn(Collections.emptyList());//mocking call to repo
        // when -actual call to the service's method
        List<User> userList = userService.getUsers();
        //then
        assertThat(userList.size()).isEqualTo(0);
    }

    // JUnit test for getUserById method
    @DisplayName("JUnit test for getUserById method")
    @Test
    void  givenUserId_whenGetUserById_thenReturnUserObject() {
        User user=UserServiceHelper.getDummyUser();
        // given
        when(userRepository.findById(1L)).thenReturn((Optional<User>) Optional.of(user));
        // when
        User savedUser = userService.getUserById(user.getId());
        // then
        assertThat(savedUser).isNotNull();
        assertEquals(user.getFirstName(),savedUser.getFirstName());
    }

    // JUnit test for updateUserDetail method
    @DisplayName("JUnit test for updateUserDetail method")
    @Test
    void givenUserDetailUpdateObjectAnduserId_whenUpdateUserDetail_thenReturnUpdatedUser() {

        //given
        User userBeforeUpdate=UserServiceHelper.getDummyUser(); // user id with 1 and data of Kavita
        User userAfterUpdate=UserServiceHelper.getDummyUser2(); // user id with 1 and updated to Nidhi

        when(userRepository.findById(1L)).thenReturn(Optional.of(userBeforeUpdate));//mocking repo call
        ArgumentCaptor<User> argument = ArgumentCaptor.forClass(User.class);//defining ArgumentCaptor for User class
        when(userRepository.save(argument.capture())).thenReturn(userAfterUpdate);//captures argument of save() which is the updated user - Nidhi's details

        //when
        UserDetailUpdateDto userDto=new UserDetailUpdateDto();//to be passed in actual call
        userDto.setFirstName("Nidhi");
        userDto.setLastName("Jadhav");
        userDto.setDob(LocalDate.of(1998, Month.SEPTEMBER, 14));
        userDto.setGender(Gender.FEMALE);
        userDto.setMobile("8499011250");
        userDto.setTargetSteps(15000);

        UserDetailUpdateDto storedDetails = userService.updateUserDetail(1L, userDto);

        //then
        assertEquals(userDto.getFirstName(), argument.getValue().getFirstName());
        assertNotNull(storedDetails);
        assertEquals(userAfterUpdate.getFirstName(), storedDetails.getFirstName());

    }

    // JUnit test for deleteUser method
    @DisplayName("JUnit test for deleteUser method")
    @Test
    void givenUserId_whenDeleteUser_thenDeleteUserFromRepo() {
        //given - precondition or setup
        Long userId = 1L;
        // when -  action or the behaviour that we are going test
        userService.deleteUser(userId);
        // then - verify the output
        verify(userRepository).deleteById(userId);
        verify(userRepository, times(1)).deleteById(userId);
    }

    // JUnit test for getUserByCredentialId method
    @DisplayName("JUnit test for getUserByCredentialId method")
    @Test
    void givenUserCredentialId_getUserByCredentialId_thenReturnUserDtoObject() {
        //given - user we will get using cred id while finding in repo
        User user=UserServiceHelper.getDummyUser();
        when(userRepository.findByUserCredentialId(user.getUserCredentialId())).thenReturn(Optional.of(user));
        // when - actual call to service returns UserDto type - this UserDto is same as User type (with respect to values)
        UserDto userDto=UserServiceHelper.getDummyUserDto();
        UserDto result = userService.getUserByCredentialId(userDto.getUserCredential().getUserCredId());
        // then
        assertThat(result).isNotNull();
        assertEquals(user.getFirstName(), result.getFirstName());
    }
}