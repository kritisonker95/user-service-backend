package com.apispocc.user.repository;

import com.apispocc.user.constant.City;
import com.apispocc.user.constant.Country;
import com.apispocc.user.constant.Gender;
import com.apispocc.user.constant.State;
import com.apispocc.user.dto.UserAddressDto;
import com.apispocc.user.dto.UserCredentialDto;
import com.apispocc.user.dto.UserDto;
import com.apispocc.user.dto.UserWeightDto;
import com.apispocc.user.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(MockitoExtension.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
class UserRepositoryTest {
    @Mock
    private UserRepository userRepository;
    @Spy
    private ModelMapper modelMapper;
    private User user;

    @BeforeEach
    public void setup(){
        Set<UserAddressDto> addresses = new HashSet<>();
        addresses.add(new UserAddressDto(
                1L,
                "Kanarichhina,Nougaon",
                City.ALMORA,
                State.UTTARAKHAND,
                263634,
                "Near Dhaulchhina",
                Country.INDIA));
        UserWeightDto userWeight=new UserWeightDto(
                1L,
                55L,
                53L,
                new Date(2023,03,31,20,06,05),
                new Date(2023,03,31,20,06,05));
        UserCredentialDto userCredential=new UserCredentialDto(
                1L,
                "KavitaRana24",
                "Kavita@24",
                "ranakavi2406@gmail.com");
        UserDto userDto= new UserDto(
                1L,
                "Rakshita",
                "Rana",
                LocalDate.of(1999, Month.JUNE, 24),
                Gender.FEMALE,
                "8433011250",
                12000,
                addresses,
                userWeight,
                userCredential,
                new Date(2023,03,31,20,06,05),
                new Date(2023,03,31,20,06,05));

         user = modelMapper.map(userDto, User.class);

    }
    @DisplayName("JUnit test for findByUserCredentialId method")
    @Test
    public void givenUser_whenFindByUserCredentialId_thenReturnUserCorrespondingToTheCredId() throws  Exception{
        userRepository.save(user);
        Optional<User> actualResult=userRepository.findByUserCredentialId(user.getUserCredentialId());
        assertThat(actualResult).isNotNull();
        //do isEqualTo - by creating a stub of the response of this api call
    }
}