package com.apispocc.user.controller;

import com.apispocc.user.dto.UserDetailUpdateDto;
import com.apispocc.user.dto.UserDto;
import com.apispocc.user.entity.User;
import com.apispocc.user.helper.UpdateUserDtoTest;
import com.apispocc.user.helper.UserControllerHelper;
import com.apispocc.user.helper.UserDtoTest;
import com.apispocc.user.helper.UserServiceHelper;
import com.apispocc.user.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.apispocc.user.helper.JSON2String.asJsonString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Spy
    private final ModelMapper modelMapper = new ModelMapper();

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    // to test addUser API
    @DisplayName("JUnit test for addUser method")
    @Test
    void addUserApi_PassValidUser_returnSavedUser() throws Exception {
        UserDtoTest dummyUserDtoTest=UserControllerHelper.getDummyUserDtoTest();//for mocking api call
        UserDto dummyUserDto=UserServiceHelper.getDummyUserDto();//for mocking service call - present internally
        // Setting mock call
        when(userService.saveUser(dummyUserDto)).thenReturn(dummyUserDto);
        // Mock API call and verify results
        mockMvc.perform( MockMvcRequestBuilders
                        .post("/register")
                        .content(asJsonString(dummyUserDtoTest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

    }

    // to test getAllUsers API
    @DisplayName("JUnit test for getAllUsers method")
    @Test
    void getAllUsersApi_PassNothing_returnAllUsersInDB() throws Exception {
        List<User> listOfUserDto = UserServiceHelper.getDummyUserList();
        // Setting mock call
        when(userService.getUsers()).thenReturn(listOfUserDto);
        // Mock API call and verify results
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/users")
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk());
    }

    // to test getUserById API
    @DisplayName("JUnit test for getUserById method")
    @Test
    void getUserByIdApi_PassValidUserId_returnValidUserCorrespondingToTheId() throws Exception {
        User dummyUser=UserServiceHelper.getDummyUser();
        // Setting mock call
        when(userService.getUserById(1L)).thenReturn(dummyUser);
        // Mock API call and verify results
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/users/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    // to test getUserByCredentialId API
    @DisplayName("JUnit test for getUserByCredentialId method")
    @Test
    void getUserByCredentialIdApi_PassValidUserCredId_returnUserCorrespondingToTheCredId() throws Exception {
        UserDto dummyUserDto=UserServiceHelper.getDummyUserDto();
        // Setting mock call
        when(userService.getUserByCredentialId(1L)).thenReturn(dummyUserDto);
        // Mock API call and verify results
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/users/creds/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    // to test updateDetail API
    @DisplayName("JUnit test for updateDetail method")
    @Test
    void updateDetailApi_PassValidUserUpdateDetails_returnUpdatedUserDetails() throws Exception {
        UserDetailUpdateDto dummyUserUpdateDto=UserServiceHelper.getDummyUserUpdateDto();
        UpdateUserDtoTest dummyUserUpdateDtoTest =UserControllerHelper.getDummyUserUpdateDtoTest();
        // Setting mock call
        when(userService.updateUserDetail(1L,dummyUserUpdateDto)).thenReturn(dummyUserUpdateDto);
        // Mock API call and verify results
        mockMvc.perform( MockMvcRequestBuilders
                        .put("/users/1/detail")
                        .content(asJsonString(dummyUserUpdateDtoTest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    // to test deleteUserById API
    @DisplayName("JUnit test for deleteUserById method")
    @Test
    void deleteUserByIdApi_PassValidUserId_returnSuccessString() throws Exception {
        // Setting mock call
        when(userService.deleteUser(1L)).thenReturn("User deleted");
        // Mock API call and verify results
        mockMvc.perform(MockMvcRequestBuilders
                        .delete("/users/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}