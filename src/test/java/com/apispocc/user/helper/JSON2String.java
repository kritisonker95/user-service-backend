package com.apispocc.user.helper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class JSON2String {
        public  static String asJsonString(final Object obj){
            try {
                return new ObjectMapper().writeValueAsString(obj);
            }
            catch(Exception e){
                throw new RuntimeException(e);
            }
        }

}
