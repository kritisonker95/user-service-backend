package com.apispocc.user.helper;

import com.apispocc.user.constant.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateUserDtoTest {
    private String firstName;
    private String lastName;
    private String dob;
    private Gender gender;
    private String mobile;
    private int targetSteps;
}
