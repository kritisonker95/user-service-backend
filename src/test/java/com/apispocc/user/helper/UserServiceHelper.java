package com.apispocc.user.helper;

import com.apispocc.user.constant.City;
import com.apispocc.user.constant.Country;
import com.apispocc.user.constant.Gender;
import com.apispocc.user.constant.State;
import com.apispocc.user.dto.*;
import com.apispocc.user.entity.User;
import com.apispocc.user.entity.UserAddress;
import com.apispocc.user.entity.UserWeight;
import com.mysql.cj.LicenseConfiguration;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserServiceHelper {

    //Returns a UserDto
    public static UserDto getDummyUserDto(){
        Set<UserAddressDto> addresses = new HashSet<>();
        addresses.add(new UserAddressDto(
                1L,
                "Kanarichhina,Nougaon",
                City.ALMORA,
                State.UTTARAKHAND,
                263634,
                "Near Dhaulchhina",
                Country.INDIA));

        UserWeightDto userWeight=new UserWeightDto(
                1L,
                55L,
                53L,
                new Date(2023,03,31,20,06,05),
                new Date(2023,03,31,20,06,05));

        UserCredentialDto userCredential=new UserCredentialDto(
                1L,
                "KavitaRana24",
                "Kavita@24",
                "ranakavi2406@gmail.com");

        UserDto userDto=new UserDto(
                1L,
                "Kavita",
                "Rana",
                LocalDate.of(1999, Month.JUNE, 24),
                Gender.FEMALE,
                "8433011250",
                12000,
                addresses,
                userWeight,
                userCredential,
                new Date(2023,03,31,20,06,05),
                new Date(2023,03,31,20,06,05));

        return userDto;
    }

    //Returns a User
    public static User getDummyUser(){
        Set<UserAddress> addresses = new HashSet<>();
        addresses.add(new UserAddress(
                1L,
                "Kanarichhina,Nougaon",
                City.ALMORA,
                State.UTTARAKHAND,
                263634,
                "Near Dhaulchhina",
                Country.INDIA,
                true));

        UserWeight userWeight=new UserWeight();
        userWeight.setUserWtId(2L);
        userWeight.setWtGoal(50L);
        userWeight.setWtCurrent(53L);

        User user=new User(
                1L,
                addresses,
                userWeight,
                1L,
                "Kavita",
                "Rana",
                LocalDate.of(1999, Month.JUNE, 24),
                Gender.FEMALE,
                "8433011250",
                12000,
                LocalDateTime.now(),
                true);

        return user;
    }

    public static User getDummyUser2(){
        Set<UserAddress> addresses = new HashSet<>();
        addresses.add(new UserAddress(
                1L,
                "Kanarichhina,Nougaon",
                City.ALMORA,
                State.UTTARAKHAND,
                263634,
                "Near Dhaulchhina",
                Country.INDIA,
                true));

        UserWeight userWeight=new UserWeight();
        userWeight.setUserWtId(2L);
        userWeight.setWtGoal(50L);
        userWeight.setWtCurrent(53L);

        User user=new User(
                1L,
                addresses,
                userWeight,
                1L,
                "Nidhi",
                "Jadhav",
                LocalDate.of(1998, Month.SEPTEMBER, 14),
                Gender.FEMALE,
                "8499011250",
                15000,
                LocalDateTime.now(),
                true);

        return user;
    }

    //Returns List of Users
    public static List<User> getDummyUserList() {
        //First User
        Set<UserAddress> addresses = new HashSet<>();
        addresses.add(new UserAddress(
                1L,
                "Kanarichhina,Nougaon",
                City.ALMORA,
                State.UTTARAKHAND,
                263634,
                "Near Dhaulchhina",
                Country.INDIA,
                true));

        UserWeight userWeight = new UserWeight();
        userWeight.setUserWtId(1L);
        userWeight.setWtGoal(50L);
        userWeight.setWtCurrent(53L);

        User user = new User(
                1L,
                addresses,
                userWeight,
                1L,
                "Kavita",
                "Rana",
                LocalDate.of(1999, Month.JUNE, 24),
                Gender.FEMALE,
                "8433011250",
                12000,
                LocalDateTime.now(),
                true);

        //Second User
        Set<UserAddress> addresses2 = new HashSet<>();
        addresses2.add(new UserAddress(
                2L,
                "Officer's Colony,Ramri aan Singh",
                City.BENGALURU,
                State.KARNATAKA,
                203134,
                "Near Mysore Chowk",
                Country.INDIA,
                true));

        UserWeight userWeight2=new UserWeight();
        userWeight.setUserWtId(2L);
        userWeight.setWtGoal(55L);
        userWeight.setWtCurrent(53L);

        User user2=new User(
                2L,
                addresses2,
                userWeight2,
                2L,
                "Kriti",
                "Sonker",
                LocalDate.of(1999, Month.NOVEMBER, 28),
                Gender.FEMALE,
                "8422011250",
                15000,
                LocalDateTime.now(),
                true
        );
        List<User> userList= List.of(user,user2);
        return userList;
    }

    public static UserDetailUpdateDto getDummyUserUpdateDto(){
        UserDetailUpdateDto toUpdateUser=new UserDetailUpdateDto(
                "Kavita",
                "Rana",
                LocalDate.of(1999, Month.NOVEMBER, 28),
                Gender.FEMALE,
                "8433011250",
                15000
        );
        return toUpdateUser;
    }
}
