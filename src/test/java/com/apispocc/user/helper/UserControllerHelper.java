package com.apispocc.user.helper;

import com.apispocc.user.constant.City;
import com.apispocc.user.constant.Country;
import com.apispocc.user.constant.Gender;
import com.apispocc.user.constant.State;
import com.apispocc.user.dto.*;
import com.apispocc.user.entity.User;
import com.apispocc.user.entity.UserAddress;
import com.apispocc.user.entity.UserWeight;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserControllerHelper {
    //Returns List of Users - UserDTOs
    public static List<UserDto> getDummyUserDtoList() {
        //First User
        UserAddressDto addressDto=new UserAddressDto();
        addressDto.setStreet("Kanarichhina,Nougaon");
        addressDto.setCity(City.ALMORA);
        addressDto.setState(State.UTTARAKHAND);
        addressDto.setPincode(263624);
        addressDto.setLandmark("Near Dhaulchhina");
        addressDto.setCountry(Country.INDIA);

        Set<UserAddressDto> addressesDto = new HashSet<>();
        addressesDto.add(addressDto);

        UserWeightDto userWeightDto=new UserWeightDto();
        userWeightDto.setWtCurrent(55L);
        userWeightDto.setWtGoal(53L);

        UserCredentialDto userCredentialDto=new UserCredentialDto();
        userCredentialDto.setUsername("GauravRana1234");
        userCredentialDto.setPassword("Gaurav@12");
        userCredentialDto.setEmail("golu12@gmail.com");

        UserDto userDto=new UserDto();
        userDto.setFirstName("Gaurav");
        userDto.setLastName("Rana");
        userDto.setDob(LocalDate.of(2001, Month.APRIL, 18));
        userDto.setGender(Gender.MALE);
        userDto.setMobile("8433011250");
        userDto.setTargetSteps(20000);
        userDto.setAddresses(addressesDto);
        userDto.setUserWeight(userWeightDto);
        userDto.setUserCredential(userCredentialDto);

        //Second User
        UserAddressDto addressDto2=new UserAddressDto();
        addressDto.setStreet("Dhaulchhina,Nougaon");
        addressDto.setCity(City.ALMORA);
        addressDto.setState(State.UTTARAKHAND);
        addressDto.setPincode(263024);
        addressDto.setLandmark("Near Almora City");
        addressDto.setCountry(Country.INDIA);

        Set<UserAddressDto> addressesDto2 = new HashSet<>();
        addressesDto.add(addressDto);

        UserWeightDto userWeightDto2=new UserWeightDto();
        userWeightDto.setWtCurrent(55L);
        userWeightDto.setWtGoal(52L);

        UserCredentialDto userCredentialDto2=new UserCredentialDto();
        userCredentialDto.setUsername("KavitaRana2406");
        userCredentialDto.setPassword("Kavita@24");
        userCredentialDto.setEmail("ranakavi2406@gmail.com");

        UserDto userDto2=new UserDto();
        userDto.setFirstName("Kavita");
        userDto.setLastName("Rana");
        userDto.setDob(LocalDate.of(1999, Month.JUNE, 24));
        userDto.setGender(Gender.FEMALE);
        userDto.setMobile("8433011250");
        userDto.setTargetSteps(15000);
        userDto.setAddresses(addressesDto2);
        userDto.setUserWeight(userWeightDto2);
        userDto.setUserCredential(userCredentialDto2);

        List<UserDto> userList= List.of(userDto,userDto2);
        return userList;
    }

    //Returns a UserDtoTest
    public static UserDtoTest getDummyUserDtoTest(){
        Set<UserAddressDto> addresses = new HashSet<>();
        addresses.add(new UserAddressDto(
                1L,
                "Kanarichhina,Nougaon",
                City.ALMORA,
                State.UTTARAKHAND,
                263634,
                "Near Dhaulchhina",
                Country.INDIA));

        UserWeightDto userWeight=new UserWeightDto(
                1L,
                55L,
                53L,
                new Date(2023,03,31,20,06,05),
                new Date(2023,03,31,20,06,05));

        UserCredentialDto userCredential=new UserCredentialDto(
                1L,
                "KavitaRana24",
                "Kavita@24",
                "ranakavi2406@gmail.com");

        UserDtoTest userDtoTest=new UserDtoTest(
                1L,
                "Kavita",
                "Rana",
                LocalDate.of(1999,Month.JUNE,24).toString(),
                Gender.FEMALE,
                "8433011250",
                12000,
                addresses,
                userWeight,
                userCredential,
                new Date(2023,03,31,20,06,05),
                new Date(2023,03,31,20,06,05));

        return userDtoTest;
    }

    //Returns a UserTest
    public static UserTest getDummyUserTest(){
        Set<UserAddress> addresses = new HashSet<>();
        addresses.add(new UserAddress(
                1L,
                "Kanarichhina,Nougaon",
                City.ALMORA,
                State.UTTARAKHAND,
                263634,
                "Near Dhaulchhina",
                Country.INDIA,
                true));

        UserWeight userWeight=new UserWeight();
        userWeight.setUserWtId(2L);
        userWeight.setWtGoal(50L);
        userWeight.setWtCurrent(53L);

        UserTest userTest=new UserTest(
                1L,
                addresses,
                userWeight,
                1L,
                "Kavita",
                "Rana",
                "24-06-1999",
                Gender.FEMALE,
                "8433011250",
                12000,
                LocalDateTime.now(),
                true);

        return userTest;
    }

    // returns UpdateUserDtoTest
    public static UpdateUserDtoTest  getDummyUserUpdateDtoTest(){
        UpdateUserDtoTest toUpdateUserTest=new UpdateUserDtoTest (
                "Kavita",
                "Rana",
                LocalDate.of(1999, Month.NOVEMBER, 28).toString(),
                Gender.FEMALE,
                "8433011250",
                15000
        );
        return toUpdateUserTest;
    }

}
